
export function convertHourStringToMinute (hoursString: string) {
    const [horas, minutos] = hoursString?.split(':')?.map(Number);

    const minutesAmount = (horas * 60) + minutos;
    return minutesAmount;
}