
export function convertMinutesToHourString (minutesAmount: number) {
    const horas = Math.floor(minutesAmount / 60)

    const minutes = minutesAmount % 60

    return `${String(horas).padStart(2, "0")}:${String(minutes).padStart(2, "0")}`;
}